# py2 to py3 @ ALBA

This project provides slides for a presentation at ALBA synchrotron

The slides are written in markdown and rendered with [remark.js](https://github.com/gnab/remark)

See the [slides](https://alba-synchrotron.gitlab.io/controls-section/slides_py2to3) or their [source](public/) 

You can render them locally with:

```console
git clone https://gitlab.com/alba-synchrotron/controls-section/slides_py2to3.git
cd slides_py2to3/public/
python3 -m http.server
```

... and view them by opening `http://localhost:8000` in your web browser
